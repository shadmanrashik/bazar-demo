<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_login', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id', 30)->unique();
            $table->string('username', 100)->unique();
            // $table->string('email')->unique();
            // $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('password_reset_code', 6)->nullable();
            // $table->rememberToken();
            $table->string('user_role', 30)->default(0);
            $table->string('created_by', 30);
            $table->string('updated_by', 30);
            // $table->dateTime('created_dt_tm');
            // $table->dateTime('updated_dt_tm');
            $table->timestamps();
            $table->integer('is_active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_login');
    }
}
