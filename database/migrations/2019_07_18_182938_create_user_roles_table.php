<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('role_code', 30);
            $table->string('role_title', 100);
            $table->text('permitted_page_code');
            $table->string('created_by', 30);
            $table->string('updated_by', 30);
            // $table->dateTime('created_dt_tm');
            // $table->dateTime('updated_dt_tm');
            $table->timestamps();
            $table->integer('is_active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_roles');
    }
}
