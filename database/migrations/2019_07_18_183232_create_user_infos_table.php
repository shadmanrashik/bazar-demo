<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id', 30);
            $table->string('full_name', 200);
            $table->string('email', 100)->nullable();
            $table->string('mobile_no', 20);
            $table->string('address', 300)->nullable();
            $table->string('profile_image', 50)->nullable();
            $table->string('created_by', 30);
            $table->string('updated_by', 30);
            // $table->dateTime('created_dt_tm');
            // $table->dateTime('updated_dt_tm');
            $table->timestamps();
            $table->integer('is_active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
