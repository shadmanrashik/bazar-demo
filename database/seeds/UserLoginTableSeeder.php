<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserLoginTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_login')->insert([
            'user_id' => '0104190001',
            'username' => 'admin',
            'password' => bcrypt('1234'),
            'user_role' => '0705190001',
            'created_by' => '0104190001',
            'updated_by' => '0104190001'
        ]);

        DB::table('user_roles')->insert([
            'role_code' => '0705190001',
            'role_title' => 'Superadmin',
            'permitted_page_code' => '01,02,03',
            'created_by' => '0104190001',
            'updated_by' => '0104190001'
        ]);

        DB::table('user_infos')->insert([
            'user_id' => '0104190001',
            'full_name' => 'Shadman Rashik',
            'email' => 'shadmanrashik@gmail.com',
            'mobile_no' => '01710905655',
            'created_by' => '0104190001',
            'updated_by' => '0104190001'
        ]);
    }
}
