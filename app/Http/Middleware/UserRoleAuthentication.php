<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

class UserRoleAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $pageCodeArr)
    {
        $permittedPageCodeArr = explode(',', Auth::user()->userRole()->first()->permitted_page_code);
        for ($i = 0; $i < count($pageCodeArr); $i++) {
            if (in_array($pageCodeArr[$i], $permittedPageCodeArr)) {
                return $next($request);
            }
        }
        return redirect('/home');
    }
}
