<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home', [
            'currentPageCode' => config('page_code.HOME_PAGE')
        ]);
    }

    public function customer()
    {
        return view('admin.customer', [
            'currentPageCode' => config('page_code.CUSTOMER_PAGE')
        ]);
    }

    public function vendor()
    {
        return view('admin.vendor', [
            'currentPageCode' => config('page_code.VENDOR_PAGE')
        ]);
    }
}
