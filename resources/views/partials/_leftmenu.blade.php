<div class="col-md-3 col-sm-3 col-xs-3 left_col menu_fixed">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
        <a href="/" class="site_title"><i class="fa fa-book"></i> <span>{{ env('APP_NAME') }}</span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{ asset('images/user/img.jpg') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ Auth::user()->user_id }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <?php
                $userPermittedPageCodeArr = explode(',', Auth::user()->userRole()->first()->permitted_page_code);
                $menuArr[] = array(
                    'levelOneHeading' => 'User Management',
                    'levelOneIcon' => 'fa fa-users',
                    'levelTwoLi' => array(config('page_code.CUSTOMER_PAGE'), config('page_code.VENDOR_PAGE')),
                    'levelTwo' => array(
                        array('levelTwoCode' => config('page_code.CUSTOMER_PAGE'), 'levelTwoHeading' => 'Customer', 'levelTwoUrl' => '/customer'),
                        array('levelTwoCode' => config('page_code.VENDOR_PAGE'), 'levelTwoHeading' => 'Vendor', 'levelTwoUrl' => '/vendor')
                    )
                );
                ?>

                <ul class="nav side-menu">
                    <?php
                    foreach ($menuArr as $menu => $eachMenu) {
                       $levelOneIsactive = '';
                       $childMenuIsBlock = '';
                       $levelTwoStr = '';
                       $childMenuBlockFlag = 0;
                       foreach ($eachMenu['levelTwo'] as $levelTwo => $levelTwoElement) {
                           $currentPage = '';

                           if (in_array($levelTwoElement['levelTwoCode'], $userPermittedPageCodeArr)) {
                               if ($currentPageCode == $levelTwoElement['levelTwoCode']) {
                                   $currentPage = 'current-page';
                                   $childMenuBlockFlag = 1;
                               }
                               $levelTwoStr .= '<li class="' . $currentPage . '"><a href="' . $levelTwoElement['levelTwoUrl'] . '">' . $levelTwoElement['levelTwoHeading'] . '</a></li>';
                           }
                        }

                       if ($levelTwoStr) {
                           if ($childMenuBlockFlag) {
                               $levelOneIsactive = 'active';
                               $childMenuIsBlock = 'style="display:block"';
                           }
                           echo '<li class="' . $levelOneIsactive . '"><a><i class="' . $eachMenu['levelOneIcon'] . '"></i> ' . $eachMenu['levelOneHeading'] . ' <span class="fa fa-chevron-down"></span></a>';
                           echo '<ul class="nav child_menu" ' . $childMenuIsBlock . ' >';
                           echo $levelTwoStr;
                           echo '</ul>';
                           echo '</li>';
                       }
                   }
                    ?>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->
    </div>
</div>
