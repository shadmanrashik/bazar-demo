@extends('layouts.loginlayout')

@section('content')
<form action="{{ route('login') }}" method="POST">
    @csrf
    <h1>Login Form</h1>
    <div>
        <input type="text" name="username" class="form-control" placeholder="Username" required="required" />
    </div>
    @error('username')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div>
        <input type="password" name="password" class="form-control" placeholder="Password" required="" />
    </div>
    @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <div>
        <button type="submit" class="btn btn-default submit">Log In</button>
        <a class="reset_pass" href="#">Lost your password?</a>
    </div>

    <div class="clearfix"></div>

    <div class="separator">
        <div class="clearfix"></div>
        <br />
        <div>
            <h1><i class="fa fa-book"></i> {{ env('APP_NAME') }} </h1>
            <p>©<?php echo date('Y')?> All Rights Reserved By <b>{{ env('APP_NAME') }}</b></p>
        </div>
    </div>
</form>
@endsection
