<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bazar Management System </title>

        <!-- Bootstrap -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <!-- Font Awesome -->
        <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <!-- NProgress -->
        <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet" type="text/css">
        <!-- bootstrap-wysiwyg -->
        <link href="{{ asset('css/prettify.min.css') }}" rel="stylesheet" type="text/css">
        <!-- animate -->
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet" type="text/css">
        <!-- Custom styling plus plugins -->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css">
        <!-- data table -->
        <link href="{{ asset('css/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css">
        <!-- bootstrap-daterangepicker -->
        <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet" type="text/css">
        <!-- bootstrap-datetimepicker -->
        <link href="{{ asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet" type="text/css">



        <!-- jQuery -->
        <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/validator.js') }}"></script>
        <!-- Bootstrap -->
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- sweet alert -->
        <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}"></script>
        <!-- data table -->
        <script type="text/javascript" src="{{ asset('js/jquery.dataTables.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/dataTables.bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/dataTable.js') }}"></script>
        <!-- notify -->
        <script type="text/javascript" src="{{ asset('js/bootstrap-notify.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/daterangepicker.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
        <!-- bootstrp select -->
        <link href="{{ asset('css/bootstrap-select.css') }}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="{{ asset('js/bootstrap-select.js') }}"></script>
<!--        <script>
            var BASE_URL = '<?php // echo base_url()   ?>';
            var type = '<?php // echo $msgFlag   ?>';
            var msg = '<?php // echo $msg   ?>';
            var icon = "glyphicon glyphicon-ok";
            var title = "Success<br>";
            if (type === 'danger') {
                icon = "glyphicon glyphicon-exclamation-sign";
                title = "Failed<br>";
            }
            $(function () {
                if (type !== "") {
                    $.notify({
                        // options
                        icon: icon,
                        title: title,
                        message: msg

                    }, {
                        // settings
                        element: 'body',
                        position: null,
                        type: type,
                        allow_dismiss: true,
                        newest_on_top: false,
                        showProgressbar: false,
                        placement: {
                            from: "top",
                            align: "center"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 1031,
                        delay: 50000,
                        timer: 1000,
                        url_target: '_blank',
                        mouse_over: null,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        },
                        onShow: null,
                        onShown: null,
                        onClose: null,
                        onClosed: null,
                        icon_type: 'class',
                        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                                '<span data-notify="icon"></span> ' +
                                '<span data-notify="title">{1}</span> ' +
                                '<span data-notify="message">{2}</span>' +
                                '<div class="progress" data-notify="progressbar">' +
                                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                                '</div>' +
                                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                                '</div>'
                    });
                }
            });
        </script>-->
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                @include('partials._leftMenu')
                @include('partials._topMenu')

                <div class="right_col" role="main">
                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>@yield('pageHeading', 'Home')</h3>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <div class="row">
                                            <div id="overlay1" style="display: none">
                                                <div class="spinnera"></div>
                                            </div>
                                            @yield('content')
                                            <?php
//                                            $this->load->view($pageUrl);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @include('partials._footer')
            </div>
        </div>
        <!-- FastClick -->
        <script type="text/javascript" src="{{ asset('js/fastclick.js') }}"></script>
        <!-- NProgress -->
        <script type="text/javascript" src="{{ asset('js/nprogress.js') }}"></script>
        <!-- bootstrap-wysiwyg -->
        <script type="text/javascript" src="{{ asset('js/bootstrap-wysiwyg.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.hotkeys.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/prettify.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>

        <!-- Custom Theme Scripts -->
        <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/myScript.js') }}"></script>
    </body>
</html>
